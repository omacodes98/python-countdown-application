import datetime

user_input = input(" Enter your goal with a deadline and username separated by colon\n ")

input_list = user_input.split(":")

goal = input_list[0]
deadline = input_list[1]
Username = input_list[2]

deadline_date = datetime.datetime.strptime(deadline, "%d.%m.%Y")
# how many days from now till the deadline

today_date = datetime.datetime.today()

time_remaining = deadline_date - today_date

print(f' {Username} the time remaining till your {goal} deadline is {time_remaining.days} days')