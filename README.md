# Countdown Application

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

An application that accepts a user input of a goal and a deadline date and then prints the remaining time until that deadline 

## Technologies Used 

* Python 

* Pycharm 

* Git 

## Steps 

Step 1: Create a python file for the project 

[created python file](/images/01_create_a_python_file_for_this_projet.png)

Step 2: Import module datetime

[imported module](/images/02_import_module_datetime.png)

Step 3: Create an input to prompt user for information and save it to a variable 

[input](/images/03_create_input_to_prompt_user_for_information_and_save_it_to_a_variable.png)


Step 4: Use the split function on the variable and save this change to another variable 

[split](/images/04_use_the_split_function_on_the_variable_and_save_this_change_to_another_variable.png)

Step 5: Print to test code 

[test code](/images/05_print_to_test_code.png)

Step 6: Call datetime module and insert definition datetime because the function we need for conversion is inside that definition. After that on that daetime definition insert function strptime this will take a string representation of a date and convert it to a date.

[strptime](/images/06_call_datetime_module_and_insert_definition_datetime_because_the_function_we_need_for_conversion_is_inside_that_definition_after_that_on_that_datetime_definition_insert_function_strptime_ithis_will_take_a_string_representation_of_a_date_and_convert_.png)

Step 7: Test code to see how it will be displayed 

[test code for display](/images/08_test_display_with_print.png)

Step 8: Assign the conversion of the string to date to a variable 

[conversion](/images/09_assigning_the_conversion_of_the_string_to_date_to_a_variable.png)

Step 9: Call datetime module and definition datetime with function today to give you todays date and time, after assign the value to a variable 

[todays date](/images/10_call_datetime_module_and_definition_datetime_with_function_today_to_give_you_todays_date_and_time_and_assign_value_to_a_variable.png)

Step 10: Subtract todays date from deadline and print 

[Subtrcat today from deadline](/images/11_subtract_todays_date_from_deadline_and_print.png)

Step 11: Create a display message with print 

[display message](/images/12_create_a_display_message_with_print.png)

Step 12: If you want only days to be displayed then add .days in variablr for calculation

[days displayed](/images/13_if_you_want_only_days_to_be_displayed_then_add_dot_days_in_variable_for_caculation.png)

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/python-countdown-application.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/python-countdown-application

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.